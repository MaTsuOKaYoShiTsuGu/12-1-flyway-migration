create table branch(
    name varchar(128),
    id int primary key AUTO_INCREMENT,
    clientID int,
    foreign key (clientID) references client(id)
);