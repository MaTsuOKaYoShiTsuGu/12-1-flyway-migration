alter table contract_service_office add (
    staffID int,
    foreign key (staffID) references staff(id)
);