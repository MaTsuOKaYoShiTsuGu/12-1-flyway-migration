create table contract_service_office(
    officeID int,
    foreign key (officeID) references office(id),
    contractID int,
    foreign key (contractID) references contract(id),
    primary key (officeID,contractID)
);