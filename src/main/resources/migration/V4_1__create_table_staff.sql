create table staff(
    firstName varchar(128),
    lastName varchar (128),
    id int primary key AUTO_INCREMENT,
    officeID int,
    foreign key (officeID) references office(id)
);