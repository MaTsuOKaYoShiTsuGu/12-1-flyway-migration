create table contract(
    name varchar(128),
    id int primary key AUTO_INCREMENT,
    branchID int,
    foreign key (branchID) references client(id)
);